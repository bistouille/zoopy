'''
This file is part of « zoopy ».

« zoopy » is free software : 
You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

« zoopy » is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
« zoopy ».

If not, see <http://www.gnu.org/licenses/>.
'''

import os
import pyglet as pg
import configparser
from collections import OrderedDict as Dico

from . import bibli
from .const import *

class Commandes :
    def __init__(self) :
        d = (('haut', 65362),
             ('bas', 65364),
             ('droite', 65363),
             ('gauche', 65361),
             ('tir', 65507),
             ('pause', 112),
             )
        self._defaut = Dico(d)
        self.initialiser()

    def __getitem__(self, nom) :
        return self._commandes[nom]

    def __setitem__(self, nom, valeur) :
        assert self.verifier(self._commandes[nom], valeur)
        self._commandes[nom] = valeur

    def items(self) :
        return self._commandes

    def values(self) :
        return self._commandes.values()

    def keys(self) :
        return self._commandes.keys()

    def __iter__(self) :
        return iter(self._commandes.items())

    def verifier(self, actuel, nouveau) :
        a = (nouveau in pg.window.key._key_names,
             not [n for n in self._commandes.values() if n != actuel].count(nouveau),
            )
        return all(a) 

    def initialiser(self) :
        self._commandes = self._defaut.copy()

class Audio :
    def __init__(self) :
        self._defaut = dict(sons=True,
                            musiques=True,
                            )
        self.initialiser()

    def __getitem__(self, nom) :
        return self._audios[nom]

    def __setitem__(self, nom, valeur) :
        assert self.verifier(valeur)
        self._audios[nom] = valeur

    def items(self) :
        return self._audios.items()

    def values(self) :
        return self._audios.values()

    def keys(self) :
        return self._audios.keys()

    def __iter__(self) :
        return iter(self._audios.items())

    def verifier(self, valeur) :
        return type(valeur) == bool

    def initialiser(self) :
        self._audios = self._defaut.copy()
        

class Config(metaclass=bibli.Singleton) :
    def __init__(self) :
        self._conf = configparser.ConfigParser()
        self._commandes = Commandes()
        self._audio = Audio()
        self._valeursAudio = (('oui', 'yes', 'true', '1'),
                              ('non', 'no', 'false', '0'))
        if os.path.isfile(FCONFIG) :
            self._conf.read(FCONFIG)
            for cmd, _ in self._commandes :
                try :
                    v = int(self._conf['COMMANDES'][cmd])
                    self._commandes[cmd] = v
                except :
                    self._initialiser()
                    break
            else :
                for t, _ in self._audio :
                    audio = self._conf['AUDIO'][t]
                    if audio in self._valeursAudio[0] :
                        self._audio[t] = True
                    elif audio in self._valeursAudio[1] :
                        self._audio[t] = False
                    else :
                        self._initialiser()
                        break
        else :
            self._initialiser()

    def _initialiser(self) :       
        self._audio.initialiser()
        self._commandes.initialiser()
        self._conf.setdefault('AUDIO', {})
        for n, v in self._audio :
            self._conf['AUDIO'][n] = 'oui' if v else 'non' 
        self._conf['COMMANDES'] = self._commandes.items()
        self.enregistrer()

    def enregistrer(self) :
        com = ('# Ne modifiez ce fichier manuellement que si vous savez ce que vous faites.',   
               '# Si une erreur éventuelle est détectée, celui-ci sera réinitialisé aux valeurs par défaut.'
               )
        with open(FCONFIG, 'w') as f :
            f.write('\n'.join(com) + '\n' * 2)
            self._conf.write(f)
    @property
    def commandes(self) :
        return self._commandes.keys()

    @property
    def audios(self) :
        return self._audios.keys()

    @property
    def sons(self) :
        return self._audio['sons']

    @sons.setter
    def sons(self, valeur:bool) :
        self._definirAudio('sons', valeur)

    @property
    def musiques(self) :
        return self._audio['musiques']

    @musiques.setter
    def musiques(self, valeur:bool) :
        self._definirAudio('musiques', valeur)

    @property
    def haut(self) :
        return self._commandes['haut']

    @haut.setter
    def haut(self, valeur:int) :
        self._definirCommande('haut', valeur)

    @property
    def bas(self) :
        return self._commandes['bas']

    @bas.setter
    def bas(self, valeur:int) :
        self._definirCommande('bas', valeur)

    @property
    def gauche(self) :
        return self._commandes['gauche']

    @gauche.setter
    def gauche(self, valeur:int) :
        self._definirCommande('gauche', valeur)

    @property
    def droite(self) :
        return self._commandes['droite']

    @droite.setter
    def droite(self, valeur:int) :
        self._definirCommande('droite', valeur)

    @property
    def tir(self) :
        return self._commandes['tir']

    @tir.setter
    def tir(self, valeur:int) :
        self._definirCommande('tir', valeur)

    @property
    def pause(self) :
        return self._commandes['pause']

    @pause.setter
    def pause(self, valeur:int) :
        self._definirCommande('pause', valeur)

    def _definirCommande(self, nom:str, valeur:int) :
        try :
            self._commandes[nom] = valeur
        except Exception as e :
            raise type(e)(e.args[0])
        else :
            self._conf['COMMANDES'][nom] = str(valeur)
            self.enregistrer()
        
    def _definirAudio(self, nom:str, valeur:bool) :
        try :
            self._audio[nom] = valeur
        except Exception as e :
            raise type(e)(e.args[0])
        else :
            valeur = 'oui' if valeur else 'non'
            self._conf['AUDIO'][nom] = valeur
            self.enregistrer()
