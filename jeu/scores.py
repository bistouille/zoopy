'''
This file is part of « zoopy ».

« zoopy » is free software : 
You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

« zoopy » is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
« zoopy ».

If not, see <http://www.gnu.org/licenses/>.
'''

import pyglet as pg
from .const import *
from . import actions, bibli, elements


class Scores :
    def __init__(self, batch, group) :
        self._score = '0'*7
        self._labels = []

        x = TB2
        y = HAUTEUR_FENETRE - TB2
        for n in self._score :
            self._labels.append(elements.Chiffre(text='0',
                                                 x=x,
                                                 y=y,
                                                 batch=batch,
                                                 group=group))
            x += TB

    def __iadd__(self, valeur:int) :
        score = str(int(self._score) + valeur).zfill(7)
        for i, n in enumerate(self._score) :
            if n != score[i] :
                self._labels[i].text = score[i]
        self._score = score
        return self

    @property
    def score(self) :
        return int(self._score)

    def delete(self) :
        for n in self._labels :
            n.delete()


class Pop(elements.Label) :
    def __init__(self, text, x, y, batch, group) :
        super().__init__(text=text,
                         x=x,
                         y=y,
                         batch=batch,
                         color=(200, 200, 200, 255),
                         font_size=28,
                         anchor_x='center',
                         anchor_y='center',
                         group=group,
                         )


class Pops :
    def __init__(self, batch, group, destX, destY, collisions) :
        self.batch = batch
        self.group = group
        self.collisions = collisions
        self._actions = actions.Actions()
        self.destination = bibli.Point(destX, destY)
        self._pops = list()
        self._actions.schedule_interval(self.deplacer, 0.03)

    def creer(self, text, x, y) :
        pop = Pop(text=text, x=x, y=y, batch=self.batch, group=self.group)
        if x < self.destination.x :
            f = self._supX
        else :
            f = self._infX
        self._actions.schedule_once(self._ajouter, 0.3, pop, f)

    def _ajouter(self, t, pop, f) :
        self._pops.append((pop, f))

    def _supX(self, x) :
        return x >= self.destination.x + 1

    def _infX(self, x) :
        return x <= self.destination.x - 1

    def deplacer(self, t) :
        for i, (label, f) in enumerate(self._pops) :
            p = bibli.deplacement(label, self.destination)
            label.x += p.x
            label.y += p.y
            if f(label.x) or self.destination.y <= label.y :
                self.collisions(int(label.text))
                label.delete()
                self._pops.pop(i)

    def delete(self) :
        self._actions.unschedule()
        for n in self._pops :
            n.delete()
