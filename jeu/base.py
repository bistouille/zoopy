'''
This file is part of « zoopy ».

« zoopy » is free software : 
You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

« zoopy » is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
« zoopy ».

If not, see <http://www.gnu.org/licenses/>.
'''

import pyglet as pg

from . import (actions,
               audio,
               bibli,
               compteur,
               config,
               elements,
               joueur,
               menu,
               pieces,
               resultats,
               ressources,
               scores,
               options,
               )
from .const import *


class ZoopyScore :
    def __init__(self, zoopy,) :
        self.z = zoopy
        self.elements = zoopy.elements
        self.indexEdition = bibli.Scores.dernier
        self._colonnes = (('', .1),
                          ('nom', .3),
                          ('score', .3),
                          ('temps', .3),
                          )
        self._y = int(HAUTEUR_FENETRE - 187 * ECHELLE) # haut. zone titre 187
        self._baseV = int(47 * ECHELLE) # haut. zone item 47
        h = round(LARGEUR_FENETRE * 0.3)
        self._baseH = (LARGEUR_FENETRE - h * 3, h, h, h)
        self._scores = bibli.Scores()
        self._sons = audio.Sons()
        self.scores = self._scores.scores
        self.elements.fond = elements.FondScores(batch=self.z.batch,
                                                 group=self.z.groupes['fond'],
                                                 )

        for n in ('titre', 'colonnes', 'lignes', 'menu') :
            getattr(self, 'creer' + n.capitalize())()

        if self.indexEdition != -1 :
            self.z.push_handlers(on_key_release=self.inscrire,)
        else :
            self.elements.menu.selection = 0
            self.z.push_handlers(on_key_release=self.elements.menu.on_key_release,)

    def _caracteres(self, sym, mod) :
        c = None
        if pg.window.key.A <= sym <= pg.window.key.Z or pg.window.key._0 <= sym <= pg.window.key._9 :
            c = sym
        elif pg.window.key.NUM_0 <= sym <= pg.window.key.NUM_9 and mod & pg.window.key.MOD_NUMLOCK :
            c = sym - pg.window.key.NUMLOCK - 1 # ?
        return c

    def inscrire(self, sym, mod) :
        car = self._caracteres(sym, mod)
        if self._edition.indexActuel <= self._edition.indexMax and car :
            self._sons.clic.play()
            self._edition.inserer(chr(car))
        elif self._edition.indexActuel > 0 and sym == pg.window.key.BACKSPACE :
            self._sons.clic.play()
            self._edition.supprimer()
        elif sym == pg.window.key.ENTER :
            self._sons.clic.play()
            self._edition.color = (22, 22, 22, 255)
            self._edition.desactiver()
            self._scores.changerNom(self._edition.text, self.indexEdition)
            self.z.remove_handler('on_key_release', self.inscrire)
            self.elements.menu.selection = 0
            self.z.push_handlers(on_key_release=self.elements.menu.on_key_release,)

    def creerMenu(self) :
        items = (('menu', self.z.menu),
                 ('quitter', self.z.quitter))
        self.elements.menu = elements.Menu(items=items,
                                           batch=self.z.batch,
                                           group=self.z.groupes['textes'])
        

    def creerTitre(self) :
        options = dict(text='scores',
                       x=LARGEUR_FENETRE / 2,
                       y=HAUTEUR_FENETRE - self._baseV * 1.5,
                       width=LARGEUR_FENETRE,
                       height=self._baseV * 3,
                       batch=self.z.batch,
                       group=self.z.groupes['textes'],
                       font_size=48,
                       color=(99, 99, 99, 255),
                       )
        self.elements.titre = resultats.TexteScore(**options)
                                        

    def creerColonnes(self) :
        options = dict(y=self._y - self._baseV * 0.5,
                       height=self._baseV,
                       batch=self.z.batch,
                       group=self.z.groupes['textes'],
                       font_size=24,
                       color=(22, 22, 22, 255),
                       )
        b = 0
        for n, l in self._colonnes :
            x = b + l / 2 * LARGEUR_FENETRE
            if n :
                options['text'] = n
                options['x'] = x
                options['width'] = l * LARGEUR_FENETRE
                self.elements[n] = resultats.TexteScore(**options)
            b += l * LARGEUR_FENETRE

    def creerLignes(self) :
        y = int(self._y - self._baseV * 2.5)
        for i, (nom, score, dep, fin) in enumerate(self.scores) :
            e = i == self.indexEdition
            self.itemNumero(i+1, y)
            self.itemNom(nom, y, i, e)
            self.itemScore(score, y, i)
            self.itemTemps(dep, fin, y, i)
            y -= int(2 * self._baseV) 

    def itemNumero(self, numero, y) :
        l = self._colonnes[0][1]
        cle = self._prefixe(numero)
        x =  l / 2 * LARGEUR_FENETRE
        options = dict(text=str(numero),
                       x=x,
                       y=y,
                       width=l * LARGEUR_FENETRE,
                       height=self._baseV,
                       batch=self.z.batch,
                       group=self.z.groupes['textes'],
                       font_size=24,
                       color=(22, 22, 22, 255),
                       )
        self.elements[cle] = resultats.TexteScore(**options)

    def itemNom(self, nom, y, num, edition=False) :
        x = .25 * LARGEUR_FENETRE
        cle = self._prefixe(nom, num)
        options = dict(text=nom,
                       x=x,
                       y=y,
                       width=0.3 * LARGEUR_FENETRE,
                       height=self._baseV,
                       batch=self.z.batch,
                       group=self.z.groupes['textes'],
                       font_size=24,
                       )
        if not edition :
            options['color'] = (22, 22, 22, 255)
            self.elements[cle] = resultats.TexteScore(**options)
        else :
            options['color'] = (255, 255, 255, 255)
            self._edition = resultats.TexteScoreEdition(**options)
            self.elements[cle] = self._edition

    def itemScore(self, score, y, num) :
        x = .55 * LARGEUR_FENETRE
        cle = self._prefixe(score, num)
        score = str(score).zfill(7)
        options = dict(text=score,
                       x=x,
                       y=y,
                       width=0.3 * LARGEUR_FENETRE,
                       height=self._baseV,
                       batch=self.z.batch,
                       group=self.z.groupes['textes'],
                       font_size=24,
                       color=(22, 22, 22, 255),
                       )
        self.elements[cle] = resultats.TexteScore(**options)

    def itemTemps(self, depart, fin, y, num) :
        temps = '00:{}:{}'.format(str(int((fin - depart) // 60)).zfill(2),
                                  str(int((fin - depart) % 60)).zfill(2))
        x = .85 * LARGEUR_FENETRE
        cle = self._prefixe(temps, num)
        options = dict(text=temps,
                       x=x,
                       y=y,
                       width=0.3 * LARGEUR_FENETRE,
                       height=self._baseV,
                       batch=self.z.batch,
                       group=self.z.groupes['textes'],
                       font_size=24,
                       color=(22, 22, 22, 255),
                       )
        self.elements[cle] = resultats.TexteScore(**options)

    def _prefixe(self, valeur, num=None) :
        p = 'scores_'
        if num :
            p += str(num) + '_'
        return  p + str(valeur)

    def delete(self) :
        self.z.remove_handler('on_key_release', self.elements.menu.on_key_release)


class ZoopyMenu :
    def __init__(self, zoopy) :
        self._sons = audio.Sons()
        self.z = zoopy
        self.e = self.z.elements
        self.e.fond = elements.FondMenu(batch=self.z.batch,
                                        group=self.z.groupes['fond'])
        self.e.logo = elements.Logo(batch=self.z.batch,
                                    group=self.z.groupes['elements'])
        self.e.menu = menu.Menu(batch=self.z.batch,
                                groupes=self.z.groupes)

        self.z.push_handlers(self.on_key_press,)

    def on_key_press(self, sym, mod) :
        if sym == pg.window.key.ENTER :
            self._sons.clic.play()
            if self.e.menu.selection.lower() == 'quitter' :
                self.z.quitter()
            elif self.e.menu.selection.lower() == 'jouer' :
                self.z.jouer()
            elif self.e.menu.selection.lower() == 'options' :
                self.z.options()
        else :
            self.e.menu.on_key_press(sym, mod)

    def delete(self) :
        self.z.remove_handlers(self.on_key_press)


class ZoopyOptions :
    def __init__(self, zoopy) :
        self.z = zoopy
        self.e = self.z.elements
        self.e.fond = elements.FondMenu(batch=self.z.batch,
                                        group=self.z.groupes['fond'])
        self.e.logo = elements.Logo(batch=self.z.batch,
                                    group=self.z.groupes['elements'])
        self.e.options = options.Options(batch=self.z.batch,
                                         groupes=self.z.groupes,
                                         retour=self.z.menu)

        

        self.z.push_handlers(self.on_key_press,)

    def on_key_press(self, sym, mod) :
        self.e.options.on_key_press(sym, mod)

    def delete(self) :
        self.z.remove_handlers(self.on_key_press)
        

INIT = 0
DEBUT = 1
NIVEAU_INFO = 2
NIVEAU_FIN = 3
NIVEAU_TRANSITION = 4


class ZoopyJeu :
    def __init__(self, zoopy) :
        self.z = zoopy
        self.e = self.z.elements
        self._actions = actions.Actions()
        self._sons = audio.Sons()
        self._valeursScore = (100, 300, 600, 1000, 1500, 2100, 2800)
        self._bonusScore = (1000, 3000)

        self._depart = False
        self._pause = False
        self._termine = False
        self._niveauTermine = False
        self.niveau = 1
        self._tempsDepart = bibli.timestamp()
        self._config = config.Config()        

        self._orientationJoueur = None
        self._eliminations = 0
        self._tirs = None
        self._etape = None
        self.initialiser()

    def initialiser(self) :
        self._etape = INIT
        self.e.pieces = pieces.Pieces(batch=self.z.batch,
                                      group=self.z.groupes['elements'])

        self.e.scores = scores.Scores(batch=self.z.batch,
                                      group=self.z.groupes['elements'])

        self.e.compteur = compteur.Compteur(valeur=NIVEAUX[self.niveau][1],
                                            batch=self.z.batch,
                                            group=self.z.groupes['elements'])

        self.e.joueur = joueur.Joueur(batch=self.z.batch,
                                      groupes=self.z.groupes,
                                      collisions=self.collisionsJoueurPieces,)

        self.e.niveau = elements.Niveau(batch=self.z.batch,
                                        group=self.z.groupes['elements'])

        self.e.fond = elements.FondJeu(batch=self.z.batch,
                                       group=self.z.groupes['fond'])

        self.e.pops = scores.Pops(batch=self.z.batch,
                                  group=self.z.groupes['elements'],
                                  destX=TB * 5.5,
                                  destY=HAUTEUR_FENETRE-TB2,
                                  collisions=self.collisionsScoresPop
                                  )

        self.musique = audio.Musiques(MUSIQUES_NIVEAUX)

        self.z.push_handlers(self.on_key_press,
                             self.on_key_release,
                             self.on_hide,
                             self.on_show,
                             )
        self._demarrerNiveau()

    def _demarrerNiveau(self, t=None, d=False) :
        if d :
            self._etape = DEBUT
            self.e.infoNiveau.delete()
            self.e.compteur.initialiser(NIVEAUX[self.niveau][1])
            self.e.joueur.actif = True
            self.e.joueur.visible = True
            self.musique.jouer()
            self._actions.schedule_interval(self.creerPiece,
                                            NIVEAUX[self.niveau][2])
        else :
            self.e.joueur.visible = False
            self._afficherInfoNiveau()
            
    def _afficherInfoNiveau(self) :
        self._etape = NIVEAU_INFO
        niveau = 'niveau {}'.format(NIVEAUX[self.niveau][0])
        self.e.niveau.changer(self.niveau)
        self.e.niveau.montrer()
        self._sons.debutNiveau.play()
        self.e.infoNiveau = elements.Information(text=niveau,
                                                 batch=self.z.batch,
                                                 groupes=self.z.groupes)
        self._actions.schedule_once(self._demarrerNiveau, 2, True)

    def _actualiserScores(self, t=None) :
        self.e.pops.creer(str(self._valeursScore[self._eliminations]),
                          self.e.joueur.x,
                          self.e.joueur.y
                         )
        d = False
        if self.e.compteur > 0 and not self._niveauTermine :
            self.e.compteur -= 1
            d = True
            if self.e.compteur == 0 :
                self._niveauTermine = True

        if d and self._niveauTermine :
            self._actions.unschedule(self.creerPiece)
            self.e.joueur.actif = False
            self.musique.arreter()
            self.musique.suivant()
            
            self._sons.niveau.play()
            self.e.niveauFini = elements.Information(text='niveau réussi',
                                                     batch=self.z.batch,
                                                     groupes=self.z.groupes)
            self.e.niveau.cacher()
            self._etape = NIVEAU_FIN
            self._actions.schedule_once(self._transitionNiveau, 2)

    def _transitionNiveau(self, t) :
        self._etape = NIVEAU_TRANSITION
        self.e.joueur.visible = False
        self.e.niveauFini.delete()
        self._tempsSuppression = self.e.pieces.vider() + 1.5
        self._actions.schedule_once(self.niveauSuivant, self._tempsSuppression)

    def on_key_press(self, sym, mod) :
        if self.e.joueur.visible and not self._pause and not self._termine :
            self.e.joueur.on_key_press(sym, mod)

    def on_key_release(self, sym, mod) :
        if sym == self._config.pause :
            if not self._pause :
                self.mettreEnPause()
            else :
                self.reprendre()
        if self.e.joueur.visible and not self._pause and not self._termine :
            self.e.joueur.on_key_release(sym, mod)

    def on_hide(self) :
        self.mettreEnPause()

    def on_show(self) :
        pass

    def mettreEnPause(self) :
        if self._termine or self._pause :
            return
        self._pause = True
        self.e.pause = elements.Information(text='jeu en pause',
                                            batch=self.z.batch,
                                            groupes=self.z.groupes)

        if self._etape == DEBUT :
            self._actions.unschedule(self.creerPiece)
            self.e.joueur.actif = False
            self.e.joueur.visible = False
            self.e.pieces.cacher()
            self.musique.arreter()
        elif self._etape == NIVEAU_FIN :
            self._actions.unschedule(self._transitionNiveau)
            self.e.niveauFini.visible = False
        elif self._etape == NIVEAU_INFO :
            self._actions.unschedule(self._demarrerNiveau)
            self.e.infoNiveau.visible = False
        elif self._etape == NIVEAU_TRANSITION :
            self._actions.unschedule(self.niveauSuivant)

    def reprendre(self) :
        self.e.pause.delete()
        if self._etape == DEBUT :
            self.e.joueur.visible = True
            self.e.joueur.actif = True
            self.e.pieces.montrer()
            self.musique.jouer()
            self._actions.schedule_interval(self.creerPiece,
                                            NIVEAUX[self.niveau][2])
        elif self._etape == NIVEAU_FIN :
            self.e.niveauFini.visible = True
            self._actions.schedule_once(self._transitionNiveau, 2)
        elif self._etape == NIVEAU_INFO :
            self._actions.schedule_once(self._demarrerNiveau, 2, True)
            self.e.infoNiveau.visible = True
        elif self._etape == NIVEAU_TRANSITION :
            self._actions.schedule_once(self.niveauSuivant,
                                        self._tempsSuppression)
        self._pause = False

    def _enregistrerScore(self) :
        if self.e.scores.score :
            scores = bibli.Scores()
            scores.enregistrer(self.e.scores.score,
                               self._tempsDepart,
                               bibli.timestamp())
        
    def creerPiece(self, dt=None) :
        #return
        rangee = self.e.pieces.creer(NIVEAUX[self.niveau][3])       
        pieceMax = 5 if 0 <= rangee <= 3 or 8 <= rangee <= 11 else 7
        if len(self.e.pieces.pieces(rangee)) > pieceMax :
            self._termine = True
            self._actions.unschedule(self.creerPiece)
            self.musique.delete()
            self._enregistrerScore()
            self.e.joueur.actif = False
            self.e.feu = elements.Feu(batch=self.z.batch,
                                      group=self.z.groupes['selements'],)
            couleur = self.e.pieces.pieces(rangee)[-1].couleur
            self.e.centre = elements.CouleurCentre(couleur=couleur,
                                                   batch=self.z.batch,
                                                   group=self.z.groupes['sfond'],)
                                            
            self.e.joueur.tuer()
            self._sons.feu.play()
            self._actions.schedule_once(self._afficherPerdu, 4.)
            # Jeu terminé
            self._actions.schedule_once(self._retourScore, 7.)

    def _afficherPerdu(self, t) :
        self.e.joueur.visible = False
        self.e.feu.visible = False
        self.e.centre.visible = False
        self._sons.perdu.play()
        self.e.perdu = elements.Information(text='vous avez perdu',
                                            batch=self.z.batch,
                                            groupes=self.z.groupes)
        
    def _retourScore(self, t) :
        self.z.scores()

    def collisionsJoueurPieces(self, rangee) :
        pc = self.e.joueur.dirs[self.e.joueur.pos]
        pieces = self.e.pieces.pieces(pc[rangee])

        retour = False

        if self.e.joueur.tirs != self._tirs :
            self._eliminations = 0
            self._tirs = self.e.joueur.tirs
            self._orientationJoueur = self.e.joueur.orientation

        for piece in pieces[::-1] :
            collision = False
            if self.e.joueur.orientation in (E, O) :
                dif = piece.width / 2
                x, x2 = piece.x - dif, piece.x + dif
                if x <= self.e.joueur.x <= x2 :
                    collision = True
            else :
                dif = piece.height / 2
                y, y2 = piece.y - dif, piece.y + dif
                if y <= self.e.joueur.y <= y2 :
                    collision = True
            if not collision :
                break
            if piece.couleur == self.e.joueur.couleur :
                self.e.pieces.supprimer(pc[rangee], piece)
                self._sons.heurt.play()
                self._actualiserScores()
                self._eliminations += 1
            else :
                self.e.joueur.couleur, piece.couleur = piece.couleur, self.e.joueur.couleur
                piece.image = ressources.images.pieces[piece.couleur]
                self._sons.echange.play()
                retour = True
                break
        return retour

    def collisionsScoresPop(self, valeur:int) :
        self.e.scores += valeur
        self._sons.score.play()
        
    def niveauSuivant(self, t=None) :
        self.niveau += 1
        self._niveauTermine = False
        if self.niveau <= 9 :
            self._demarrerNiveau()
        else :
            self._termine = True
            self.musique.delete()
            self._enregistrerScore()
            self._sons.gagne.play()
            self.e.gagne = elements.Information(text='bravo, vous avez réussi !',
                                                batch=self.z.batch,
                                                groupes=self.z.groupes)
            self._actions.schedule_once(self._retourScore, 4)

    def delete(self) :
        self._actions.unschedule()
        self.z.remove_handlers(self.on_key_press,
                               self.on_key_release,
                               self.on_hide,
                               self.on_show,
                             )
