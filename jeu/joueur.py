'''
This file is part of « zoopy ».

« zoopy » is free software : 
You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

« zoopy » is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
« zoopy ».

If not, see <http://www.gnu.org/licenses/>.
'''

import random

from .const import *
from . import actions, audio, bibli, config, elements, ressources

RDELAI = 0.09

class TraineeJoueur(elements.Sprite) :
    def __init__(self, x, y, couleur, orientation, batch, group) :
        self.couleur = couleur
        #self._actions = actions.Actions()
        img = ressources.images.joueursTrainees[self.couleur]
        # L'anim pivote sur sa coord. bg (anchor fonctionne pas sur image grid)
        x -= TB2
        y -= TB2
        if orientation == N :
            y += TB
        elif orientation == E :
            x += TB
            y += TB
        elif orientation == S :
            x += TB
        anim = elements.animation(image=img,
                                 lignes=1,
                                 colonnes=9,
                                 largeur=64,
                                 hauteur=64,
                                 delai=.03)
        super().__init__(anim,
                         x=x,
                         y=y,
                         batch=batch,
                         group=group,
                         )
        self.orientation = O
        self.pivoter(orientation)
    
    def pivoter(self, orientation) :
        if self.orientation != orientation :
            dif = self.orientation - orientation
            self.rotation -= dif
            self.orientation = orientation


class Trainee :
    def __init__(self, group, batch, couleur, orientation) :
        self.group = group
        self.batch = batch
        self.couleur = couleur
        self.orientation = orientation
        self._trainee = {}

    def creer(self, position) :
        self.position = bibli.Point(*position)
        if self.orientation in (N, S) :
            n, m = self.position.y, self._creerY
        else :
            n, m = self.position.x, self._creerX
        for c in self._trainee :
            if c[0] <= n <= c[1] :
                if not self._trainee[c] :
                    self._trainee[c] = m(c[0])
                break

    def _creerX(self, x) :
        return self._creerTrainee(x=x, y=self.position.y)

    def _creerY(self, y) :
        return self._creerTrainee(x=self.position.x, y=y)

    def _creerTrainee(self, x, y) :
        return TraineeJoueur(x=x,
                             y=y,
                             couleur=self.couleur,
                             orientation=self.orientation,
                             batch=self.batch,
                             group=self.group)

    def initialiser(self, dep, des, vider=True) :
        if vider :
            self._vider()
        if self.orientation in (O, S) :
            for n in range(int(dep), int(des)-1, -TB) :
                self._trainee[(n , n+TB-1)] = None
        else :
            for n in range(int(dep), int(des)+1, TB) :
                self._trainee[(n, n+TB-1)] = None

    def _vider(self) :
        for n in self._trainee.values() :
            if n :
                n.delete()
        self._trainee.clear()


class Joueur(elements.Sprite) :
    def __init__(self, batch, groupes, collisions, position:int=6) :
        self._base = TB
        self._actions = actions.Actions()
        self._groupes = groupes
        self._config = config.Config()
        self._sons = audio.Sons()
        self.collisions = collisions
        self._coords = []
        y = self._base * 5.5
        for n in range(20) :
            if not n % 4 :
                x = self._base * 7.5
                if n :
                    y += self._base
            self._coords.append((x, y))
            x += self._base

        self.dirs = ((0,7,11,12),
                     (1,7,10,12),
                     (2,7,9,12),
                     (3,7,8,12),
                     (0,6,11,13),
                     (1,6,10,13),
                     (2,6,9,13),
                     (3,6,8,13),
                     (0,5,11,14),
                     (1,5,10,14),
                     (2,5,9,14),
                     (3,5,8,14),
                     (0,4,11,15),
                     (1,4,10,15),
                     (2,4,9,15),
                     (3,4,8,15))

        d = ('haut', 'bas', 'gauche', 'droite')
        self._touches = tuple(getattr(self._config, n) for n in d)
        self.tirs = 0
        self._tirActif = False
        self.pos = position
        self._couleur = random.choice(COULEURS)
        self.orientation = O
        self.vitesse = 1 / 60
        self.pasTir = TB * 0.75
        x, y = self._coords[self.pos]
        super().__init__(ressources.images.joueurs[self._couleur],
                         x=x,
                         y=y,
                         batch=batch,
                         group=self._groupes['elements'],
                         )
        self.visible = False
        self.actif = False
        self._trainee = Trainee(batch=batch,
                                group=self._groupes['selements'],
                                couleur=self._couleur,
                                orientation=self.orientation)

    @property
    def couleur(self) :
        return self._couleur

    @couleur.setter
    def couleur(self, couleur) :
        self.image = ressources.images.joueurs[couleur]
        self._couleur = couleur
        self._trainee.couleur = couleur
        self.pivoter(self.orientation)

    def _deplacer(self, t, direction) :
        if self._tirActif :
            return
        if direction == self._config.haut :
            if self.pos < 12 :
                self.pos += 4
                self.x, self.y = self._coords[self.pos]
            self.pivoter(N)
        elif direction == self._config.bas :
            if self.pos > 3 :
                self.pos -= 4
                self.x, self.y = self._coords[self.pos]
            self.pivoter(S)
        elif direction == self._config.gauche :
            b = bibli.bornes(self.pos, 4)
            if self.pos > b[0] :
                self.pos -= 1
                self.x, self.y = self._coords[self.pos]
            self.pivoter(O)
        elif direction == self._config.droite :
            b = bibli.bornes(self.pos, 4)
            if self.pos < b[1] :
                self.pos += 1
                self.x, self.y = self._coords[self.pos]
            self.pivoter(E)

    def on_key_press(self, sym, mod) :
        if not self.actif :
            return
        if sym in self._touches :
            self._actions.schedule_interval(self._deplacer, 0.1, sym)
        elif sym == self._config.tir :
            self.tirer()

    def on_key_release(self, sym, mod) :
        self._actions.unschedule(self._deplacer)

    def tirer(self, dt=None) :
        if self._tirActif :
            return
        self._sons.tir.play()
        self.tirs += 1
        self._tirActif = True
        if self.orientation == N :
            self._trainee.initialiser(self.y + TB2, HAUTEUR_FENETRE + TB2)
            self._actions.schedule_interval(self._nord,
                                            self.vitesse,
                                            self.y,
                                            HAUTEUR_FENETRE
                                            )
        elif self.orientation == S :
            self._trainee.initialiser(self.y + TB2, -TB2)
            self._trainee.creer(self.position)
            self._actions.schedule_interval(self._sud,
                                            self.vitesse,
                                            self.y,
                                            0)
        elif self.orientation == O :
            self._trainee.initialiser(self.x + TB2, -TB2)
            self._trainee.creer(self.position)
            self._actions.schedule_interval(self._ouest,
                                            self.vitesse,
                                            self.x,
                                            0)
        elif self.orientation == E :
            self._trainee.initialiser(self.x + TB2, LARGEUR_FENETRE+TB2)
            self._actions.schedule_interval(self._est,
                                            self.vitesse,
                                            self.x,
                                            LARGEUR_FENETRE)

    def _reactiverTir(self, t) :
        self._tirActif = False

    def _nord(self, t, depart, destination) :
        self.y += self.pasTir
        self._trainee.creer(self.position)
        col = self.collisions(0)
        if self.y >= destination or all((depart, col)) :
            self._actions.unschedule(self._nord)
            if depart :
                self.pivoter(S)
                d = HAUTEUR_FENETRE                
                if col :
                    d -= ((HAUTEUR_FENETRE - self.y) // TB) * TB
                self._trainee.initialiser(d, depart + TB2)
                self._trainee.creer(self.position)
                self._actions.schedule_interval(self._sud,
                                                self.vitesse,
                                                None,
                                                depart)
            else :
                self._actions.schedule_once(self._reactiverTir, RDELAI)
                
    def _sud(self, t, depart, destination) :
        self.y -= self.pasTir
        self._trainee.creer(self.position)
        col = self.collisions(2)
        if self.y <= destination or all((depart, col)) :
            self._actions.unschedule(self._sud)
            if depart :
                self.pivoter(N)
                d = 0
                if col :
                    d += ((self.y // TB) + 1) * TB
                self._trainee.initialiser(d, depart)
                self._trainee.creer(self.position)
                self._actions.schedule_interval(self._nord,
                                                self.vitesse,
                                                None,
                                                depart)
            else :
                self._actions.schedule_once(self._reactiverTir, RDELAI)

    def _ouest(self, t, depart, destination) :
        self.x -= self.pasTir
        self._trainee.creer(self.position)
        col = self.collisions(3)
        if self.x <= destination or all((depart, col)) :
            self._actions.unschedule(self._ouest)
            if depart :
                self.pivoter(E)
                d = 0
                if col :
                    d += ((self.x // TB) + 1) * TB
                self._trainee.initialiser(d, depart)
                self._trainee.creer(self.position)
                self._actions.schedule_interval(self._est,
                                                self.vitesse,
                                                None,
                                                depart)
            else :
                self._actions.schedule_once(self._reactiverTir, RDELAI)

    def _est(self, t, depart, destination) :
        self.x += self.pasTir
        self._trainee.creer(self.position)
        col = self.collisions(1)
        if self.x >= destination or all((depart, col)) :
            self._actions.unschedule(self._est)
            if depart :
                self.pivoter(O)
                d = LARGEUR_FENETRE
                if col :
                    d -= ((LARGEUR_FENETRE - self.x) // TB) * TB
                self._trainee.initialiser(d, depart)
                self._trainee.creer(self.position)
                self._actions.schedule_interval(self._ouest,
                                                self.vitesse,
                                                None,
                                                depart)
            else :
                self._actions.schedule_once(self._reactiverTir, RDELAI)

    def pivoter(self, orientation) :
        if self.orientation != orientation :
            dif = self.orientation - orientation
            self.rotation -= dif
            self.orientation = orientation
            self._trainee.orientation = orientation

    def tuer(self) :
        o = bibli.Point(*self.position)
        d = bibli.Point(LARGEUR_FENETRE / 2, HAUTEUR_FENETRE / 2)
        m = o.x - d.x
        def deplacer(t) :
            o =  bibli.Point(*self.position)
            p = bibli.deplacement(o, d, pas=TB / 8)
            if m > 0 and self.x + p.x < d.x or m < 0 and self.x + p.x > d.x :
                self._actions.unschedule(deplacer)
                self.image = ressources.images.joueursMort[self._couleur]
                self.pivoter(self.orientation)
            self.x += p.x
            self.y += p.y
        self._actions.schedule_interval(deplacer, 1/FPS)

    def delete(self) :
        self._actions.unschedule()
        super().delete()
