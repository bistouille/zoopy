'''
This file is part of « zoopy ».

« zoopy » is free software : 
You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

« zoopy » is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
« zoopy ».

If not, see <http://www.gnu.org/licenses/>.
'''

import pyglet as pg
from .const import *
from . import elements


class Compteur :
    def __init__(self, valeur, batch, group) :
        self._batch = batch
        self._group = group
        self._chiffres = []
        self.initialiser(valeur)

    def initialiser(self, valeur) :
        self.delete()
        self._chiffres.clear()
        self._longueur = len(str(valeur))
        self._compteur = valeur
        x = LARGEUR_FENETRE - TB2
        y = HAUTEUR_FENETRE - TB2
        for n in str(self._compteur)[::-1] :
            self._chiffres.append(elements.Chiffre(x=x,
                                                   y=y,
                                                   text=n,
                                                   batch=self._batch,
                                                   group=self._group))
            x -= TB

    def _actualiser(self) :
        for i, n in enumerate(str(self._compteur).zfill(self._longueur)[::-1]) :
            self._chiffres[i].text = n

    def __isub__(self, valeur:int) :
        self._compteur -= valeur
        if self._compteur < 0 :
            self._compteur = 0
        self._actualiser()
        return self

    def __eq__(self, valeur:int) :
        return self._compteur == valeur

    def __ne__(self, valeur:int) :
        return self._compteur != valeur
        
    def __le__(self, valeur:int) :
       return self._compteur <= valeur
    
    def __lt__(self, valeur:int) :
        return self._compteur < valeur
        
    def __ge__(self, valeur:int) :
        return self._compteur >= valeur
        
    def __gt__(self, valeur:int) :
        return self._compteur > valeur

    def __str__(self) :
        return str(self._compteur)

    def delete(self) :
        for n in self._chiffres :
            n.delete()
