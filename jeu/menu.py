'''
This file is part of « zoopy ».

« zoopy » is free software : 
You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

« zoopy » is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
« zoopy ».

If not, see <http://www.gnu.org/licenses/>.
'''

import pyglet as pg

from .const import *
from . import actions, audio, elements, ressources

class itemMenu() :
    def __init__(self, text, x, y, batch, groupes) :
        self._actions = actions.Actions()
        self.bouton = elements.Sprite(ressources.images.boutons['normal'],
                                      x=x,
                                      y=y,
                                      batch=batch,
                                      group=groupes['elements'],
                                      )
        self.bouton.anchor_x = x - self.bouton.width / 2

        self.texte = elements.Label(text=text.upper(),
                                    x=x,
                                    y=y,
                                    batch=batch,
                                    color=(18, 18, 18, 255),
                                    font_size=28,
                                    width=self.bouton.width,
                                    height=self.bouton.height,
                                    anchor_x='center',
                                    anchor_y='center',
                                    group=groupes['textes'],
                                    )
        self.texte.content_valign = 'center'
        self.couleurs = ((18, 18, 18, 255),
                         (66, 66, 66, 255),
                         )

    def focus(self) :
        self.bouton.image = ressources.images.boutons['focus']
        self.texte.color = self.couleurs[1]
        self._actions.schedule_interval(self._animer, 0.5)

    def _animer(self, t) :
        if self.texte.color == self.couleurs[0] :
            self.bouton.image = ressources.images.boutons['focus']
            self.texte.color = self.couleurs[1]
        else :
            self.bouton.image = ressources.images.boutons['normal']
            self.texte.color = self.couleurs[0]

    def normal(self) :
        self._actions.unschedule(self._animer)
        self.texte.color = self.couleurs[0]
        self.bouton.image = ressources.images.boutons['normal']

    def cacher(self) :
        self.text.visible = False
        self.bouton.visible = False

    def montrer(self) :
        self.text.visible = True
        self.bouton.visible = True

    def delete(self) :
        self._actions.unschedule()
        self.texte.delete()
        self.bouton.delete()


class Menu :
    def __init__(self, batch, groupes) :
        self._actions = actions.Actions()
        self._sons = audio.Sons()
        x = LARGEUR_FENETRE / 2
        y = [int(n * ECHELLE) for n in (600, 450, 300)]

        self._items = (itemMenu('jouer', x, y[0], batch, groupes),
                       itemMenu('options', x, y[1], batch, groupes),
                       itemMenu('quitter', x, y[2], batch, groupes),
                       )
        self._focus = 0
        self._tab = True
        self._focus = 0
        self._items[0].focus()
        
    def _temporiser(self) :
        self._actions.schedule_once(self._inverserEtat, 0.1)

    def _inverserEtat(self, t) :
        self._tab = not self._tab
        
    def on_key_press(self, sym, mod) :
        if self._tab and sym == pg.window.key.TAB :
            self._sons.nav.play()
            self._tab = False
            self._temporiser()
            self._suivant()

    def on_key_release(self, sym, mod) :
        pass

    def _suivant(self) :
        self._items[self._focus].normal()
        self._focus += 1
        if self._focus > len(self._items) - 1 :
            self._focus = 0
        self._items[self._focus].focus()

    @property
    def selection(self) :
        return self._items[self._focus].texte.text

    def delete(self) :
        self._actions.unschedule()
        for item in self._items :
            item.delete()
