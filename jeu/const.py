'''
This file is part of « zoopy ».

« zoopy » is free software : 
You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

« zoopy » is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
« zoopy ».

If not, see <http://www.gnu.org/licenses/>.
'''

import os

#XXX
# L'utilisation de pluseaudio par pyglet provoque des erreurs de segmentation sur
# mon système Debian, ou encore ce genre d'erreur :
# Traceback (most recent call last):
# File "_ctypes/callbacks.c", line 277, in 'calling callback function'
# TypeError: _ajouter() missing 1 required positional argument: 'f'
# Assertion '!m->thread || !pa_thread_is_running(m->thread) || !in_worker(m)' failed at pulse/thread-mainloop.c:166, function pa_threaded_mainloop_lock(). Aborting.
# Abandon

AUDIO = ('openal', 'directsound', 'silent', 'pulse')
FPS = 60
N = 0
E = 90
S = 180
O = 270

COULEURS = ('vert', 'jaune', 'bleu', 'rouge')

LIGNES = 18
COLONNES = 14

ECHELLE = .75 # 810 × 630
TB = int(64 * ECHELLE)
TB2 = int(TB / 2)

# Base 1152 × 896
LARGEUR_FENETRE = LIGNES * TB
HAUTEUR_FENETRE = COLONNES * TB

NIVEAUX = {1:('I', 40, 1.2, .46),
           2:('II', 60, 1.15, .44),
           3:('III', 80, 1.1, .42),
           4:('IV', 100, 1.05, .40),
           5:('V', 130, 1., .38),
           6:('VI', 170, .95, .36),
           7:('VII', 220, .9, .34),
           8:('VIII', 280, .85, .32),
           9:('IX', 350, .8, .30),
           }

MUSIQUES_NIVEAUX = ('Doobly_Doo',
                    'Danger_Storm',
                    'March of the Spoons',
                    'Voltaic',
                    'Crossing_the_Chasm',
                    'Graveyard_Shift',
                    'Darkling',
                    'Obliteration',
                    'Unholy_Knight',
                    )

MUSIQUE_MENU = ('Magic Forest',)

REP = os.path.abspath(os.path.dirname(__file__)) + '/'
RRESSOURCES = REP + '../ressources/'
RIMAGES = RRESSOURCES + 'images/'
RPOLICES = RRESSOURCES + 'polices/'
RSONS = RRESSOURCES + 'sons/'
RMUSIQUES = RRESSOURCES + 'musiques/'

RDONNEES = REP + '../donnees/' 
FSCORES = RDONNEES + 'scores'
FCONFIG = RDONNEES + 'preferences'
