'''
This file is part of « zoopy ».

« zoopy » is free software : 
You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

« zoopy » is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
« zoopy ».

If not, see <http://www.gnu.org/licenses/>.
'''

import pyglet as pg
import random
from collections import deque

from .const import *
from . import actions, audio, bibli, elements, ressources


class Points :
    def __init__(self) :
        self._dirs = []
        # Du haut vers la gauche dans le sens horaire
        # Nord
        x = int(TB * 7.5)
        for i in range(21) :
            if not i % 5 :
                if i :
                    self._dirs.append(l)
                    x += TB
                l = []
                y = int(TB * 13.5)
            l.append(bibli.Point(x, y))
            y -= TB
        # Est
        y = HAUTEUR_FENETRE - int(TB * 5.5)
        for i in range(29) :
            if not i % 7 :
                if i :
                    self._dirs.append(l)
                    y -= TB
                l = []
                x = int(TB * 17.5)
            l.append(bibli.Point(x, y))
            x -= TB
        # Sud
        x = LARGEUR_FENETRE - int(TB * 7.5)
        for i in range(21) :
            if not i % 5 :
                if i :
                    self._dirs.append(l)
                    x -= TB
                l = []
                y = int(TB * 0.5)
            l.append(bibli.Point(x, y))
            y += TB
        # Ouest
        y = int(TB * 5.5)
        for i in range(29) :
            if not i % 7 :
                if i :
                    self._dirs.append(l)
                    y += TB
                l = []
                x = int(TB * 0.5)
            l.append(bibli.Point(x, y))
            x += TB

    def __getitem__(self, index) :
        return self._dirs[index]

    def __len__(self) :
        return len(self._dirs)


class Piece(elements.Sprite) :
    def __init__(self, x, y, couleur, batch, group) :
        self.couleur = couleur
        self._actions = actions.Actions()
        super().__init__(ressources.images.pieces[couleur],
                         x=x,
                         y=y,
                         batch=batch,
                         group=group)
        self._set_visible(False)
        self._evolution = self.scale / 10
        self._age = 1
        self.scale = self._evolution

    @property
    def visible(self) :
        return self._visible

    @visible.setter
    def visible(self, b) :
        self._set_visible(b)
        if self._age < 10 :
            if b :
                self._actions.schedule_interval(self._grandir, 5/FPS)
            else :
                self._actions.unschedule(self._grandir)

    def _grandir(self, t) :
        self.scale += self._evolution
        self._age += 1
        if self._age == 10 :
            self._actions.unschedule(self._grandir)

    def delete(self, t=None) :
        self._actions.unschedule()
        super().delete()


class Pieces :
    def __init__(self, batch, group) :
        self._actions = actions.Actions()
        self._sons = audio.Sons()
        self._batch = batch
        self._group = group
        self._couleurs = list(COULEURS)
        self._points = Points()
        self._nbRangees = len(self._points)
        self._rangees = []
        self._exclusionRangees = deque(maxlen=3)
        self._registre = []
        for n in range(self._nbRangees) :
            self._rangees.append([])
            if 0 <= n <= 3 or 8 <= n <= 11 :
                self._registre.append([0, 6])
            else :
                self._registre.append([0, 8])
        self._pas = TB / 8
        self._visible = True

    def creer(self, v) :
        rangee = self._selectionnerRangee()
        self._exclusionRangees.append(rangee)
        n = random.random()
        if n <= v and self._rangees[rangee] :
            couleur = self._rangees[rangee][0].couleur
        else :
            c = set(self._couleurs)
            if self._rangees[rangee] :
                c -= set((self._rangees[rangee][0].couleur,))
            c = list(c)
            random.shuffle(c)
            couleur = c[0]
        
        p = self._points[rangee][0]
        pc = Piece(p.x, p.y, couleur, self._batch, self._group)
        self._rangees[rangee].insert(0, pc)
        self._registre[rangee][0] += 1
        # Décalage des éléments suivant (précédemment placés)
        if rangee <= 3 :
            xa, ya = 0, -TB
        elif 4 <= rangee <= 7 :
            xa, ya = -TB, 0
        elif 8 <= rangee <= 11 :
            xa, ya = 0, TB
        else : # 12 , 15
            xa, ya = TB, 0

        self._actions.schedule_once(self._decaler, 3/FPS, rangee, xa, ya)
        return rangee

    def _decaler(self, t, rangee, dx, dy, ax=0, ay=0) :
        if dx == ax and dy == ay :
            if self._visible :
                try :
                    self._rangees[rangee][0].visible = True
                except IndexError :
                    pass
            return
        if dx :
            pasx = self._pas if dx > 0 else -self._pas
            pasy = 0
        else :
            pasx = 0
            pasy = self._pas if dy > 0 else -self._pas
        for n in self._rangees[rangee][1:] :
            try :
                n.x += pasx
                n.y += pasy
            except :
                return
        self._actions.schedule_once(self._decaler,
                                    3/FPS,
                                    rangee,
                                    dx,
                                    dy,
                                    ax+pasx,
                                    ay+pasy)

    def _selectionnerRangee(self) :
        dif = []
        d = 1
        l = []
        for i, (a, m) in enumerate(self._registre) :
            v = m - a
            dif.append((d, d + v - 1))
            if i not in self._exclusionRangees :
                l.extend(range(d, d + v))
            d += v
        r = random.choice(l)
        for i, (m, n) in enumerate(dif) :
            if m <= r <= n :
                break
        return i

    def supprimer(self, rangee, piece) :
        self._rangees[rangee].remove(piece)
        self._registre[rangee][0] -= 1

    def pieces(self, rangee=None) :
        if rangee != None :
            return self._rangees[rangee]
        return self._rangees

    def vider(self) :
        t = 3/FPS
        l = []
        for rangee in self._rangees :
            for n in rangee :
                l.append(n)
            rangee.clear()
        random.shuffle(l)
        for p in l :
            self._actions.schedule_once(self._supprimerPiece, t, p)
            t += 3/FPS
        for n in self._registre :
            n[0] = 0
        return t

    def _supprimerPiece(self, t, piece) :
        x, y = piece.x - piece.width, piece.y - piece.height
        img = ressources.images.explosionPieces[piece.couleur]
        piece.image = elements.animation(image=img,
                                         lignes=1,
                                         colonnes=8,
                                         largeur=64,
                                         hauteur=64,
                                         delai=.1)
        piece.x = x + TB2
        piece.y = y + TB2
        self._sons.explosionPiece.play()
        self._actions.schedule_once(piece.delete, 54/FPS)

    def _afficher(self, etat:bool) :
        for pieces in self._rangees :
            for piece in pieces :
                piece.visible = etat
    
    def cacher(self) :
        self._visible = False
        self._afficher(False)

    def montrer(self) :
        self._visible = True
        self._afficher(True)

    def delete(self) :
        self._actions.unschedule()
        for rangee in self._rangees :
            for piece in rangee :
                try :
                    piece.delete()
                except :
                    continue
