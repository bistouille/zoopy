'''
This file is part of « zoopy ».

« zoopy » is free software : 
You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

« zoopy » is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
« zoopy ».

If not, see <http://www.gnu.org/licenses/>.
'''

from . import actions, elements

class TexteScore(elements.Label) :
    def __init__(self, **dargs) :
        dargs['text'] = dargs['text'].upper()
        super().__init__(anchor_x='center',
                         anchor_y='center',
                         **dargs
                         )
        self.content_valign = 'center'


class TexteScoreEdition(TexteScore) :
    def __init__(self, **dargs) :
        self._actions = actions.Actions()
        self._caractere = dargs['text'][0]
        super().__init__(**dargs)
        self._texte = list(self.text)
        self._texteBase = list(self.text)
        self._longTexte = len(self.text)
        self.indexActuel = 0
        self.indexMax = len(self.text) - 1
        self.clignoter(self.indexActuel)

    def clignoter(self, index, etat:bool=True) :
        self._clignotement = etat
        if etat :
            self._actions.schedule_interval(self._alternerClignotement, .5, index)
        else :
            self._actions.unschedule(self._alternerClignotement)
            self.text = ''.join(self._texte)

    def _alternerClignotement(self, t, index) :
        if self.text[index] == self._texte[index] :
            t = self._texte.copy()
            t[index] = '\t'
            self.text = ''.join(t)
        else :
            self.text = ''.join(self._texte)

    def inserer(self, caractere) :
        self.clignoter(self.indexActuel, False)
        self._texte[self.indexActuel] = caractere.upper()
        self.text = ''.join(self._texte)
        if self.indexActuel < self.indexMax :
            self.indexActuel += 1
            self.clignoter(self.indexActuel)

    def supprimer(self) :
        if self.indexActuel == 0 :
            raise IndexError
        if self._clignotement :
            self.indexActuel -= 1
        self.clignoter(self.indexActuel, False)
        self._texte[self.indexActuel] = self._texteBase[self.indexActuel]
        self.text = ''.join(self._texte)
        self.clignoter(self.indexActuel)

    def desactiver(self) :
         self.clignoter(self.indexActuel, False)

    def delete(self) :
        self._actions.unschedule()
        super().delete()
