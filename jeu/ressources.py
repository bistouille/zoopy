'''
This file is part of « zoopy ».

« zoopy » is free software : 
You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

« zoopy » is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
« zoopy ».

If not, see <http://www.gnu.org/licenses/>.
'''

import pyglet as pg
import os
from collections import namedtuple

from .const import *
from . import config

conf = config.Config()

pg.resource.path = [RIMAGES]
pg.resource.reindex()

pg.font.add_directory(RPOLICES)
police = 'Armalite Rifle'
    
def imageCentree(image:str) :
    img = pg.resource.image(image)
    img.anchor_x = img.width / 2
    img.anchor_y = img.height / 2
    return img

def dimages(prefixe, couleurs, centrage=True, ext='png') :
    dico = dict.fromkeys(couleurs)
    for n in dico :
        f = prefixe + n + '.' + ext
        if centrage :
            img = imageCentree(f)
        else :
            img = pg.resource.image(f)
        dico[n] = img
    return dico

images = namedtuple('images', ('joueurs',
                               #'tirs',
                               'joueursTrainees',
                               #'joueurExplosion',
                               'joueurMort',
                               'pieces',
                               'explosionPieces',
                               'menu',
                               'scores',
                               'plateau',
                               'logo',
                               'boutons',
                               'controles',
                               'feu',
                               'icone',
                               'fondsCentre',
                               )
                    )

images.joueurs = dimages('joueur_', COULEURS)
images.joueursTrainees = dimages('trainee_', COULEURS, centrage=False)
images.joueursMort = dimages('mort_', COULEURS)
#images.joueurExplosion = pg.resource.image('joueur_explosion.png')
images.pieces = dimages('piece_', COULEURS)
images.explosionPieces = dimages('explosion_', COULEURS, centrage=False)
images.fondsCentre = dimages('fond_centre_', COULEURS)
images.boutons = dict(normal=imageCentree('bouton_normal.png'),
                      focus=imageCentree('bouton_focus.png'),
                      interrupteur=imageCentree('bouton_interrupteur.png'),
                      )
images.controles = dict(normal=imageCentree('controles_normal.png'),
                        focus=imageCentree('controles_focus.png'),
                        bords=imageCentree('controles_bords.png'),
                        )

images.menu = pg.resource.image('menu.jpg')
images.scores = pg.resource.image('score.jpg')
images.plateau = pg.resource.image('plateau.jpg')
images.logo = imageCentree('logo.png')
images.feu = pg.resource.image('feu_centre.png')
images.icone = pg.resource.image('icone_32.png')
