'''
This file is part of « zoopy ».

« zoopy » is free software : 
You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

« zoopy » is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
« zoopy ».

If not, see <http://www.gnu.org/licenses/>.
'''

import pyglet as pg

from .const import *
from . import audio, config, elements, ressources

class itemOption(elements.Sprite) :
    def __init__(self, groupes, **dargs) :
        dargs['group'] = groupes['elements']
        super().__init__(ressources.images.boutons['interrupteur'], **dargs)
        self.x = LARGEUR_FENETRE / 2 + self.width / 2# + decalage * ECHELLE
        o = (('o', 'I', self.x - self.width * ECHELLE / 4, (200, 255, 200, 255), 16),
             ('n', '0', self.x + self.width * ECHELLE / 4, (33, 33, 33, 33), int(16*0.75))
             ,)

        toptions = dict(y=self.y,
                        batch=self.batch,
                        width=self.width,
                        height=self.height,
                        anchor_x='center',
                        anchor_y='center',
                        group=groupes['textes'],
                        )
        for n, t, x, c, s in o :
            e = elements.Label(text=t.upper(),
                               x=x,
                               color=c,
                               font_size=s,
                               **toptions
                               )
            setattr(self, n, e)
            getattr(self, n).content_valign = 'center'
        self.actif = True

    def activer(self) :
        if self.actif :
            return
        self.actif = True
        self.rotation = 0
        self.o.color, self.n.color = self.n.color, self.o.color
        self.o.font_size, self.n.font_size = self.n.font_size, self.o.font_size

    def desactiver(self) :
        if not self.actif :
            return
        self.actif = False
        self.rotation = 180
        self.o.color, self.n.color = self.n.color, self.o.color
        self.o.font_size, self.n.font_size = self.n.font_size, self.o.font_size

    def delete(self) :
        self.o.delete()
        self.n.delete()
        super().delete()


class itemOptionControles(elements.Sprite) :
    def __init__(self, action, commande, groupes, **dargs) :
        dargs['group'] = groupes['elements']
        super().__init__(ressources.images.controles['normal'], **dargs)
        self.action = elements.Label(text=action.upper(),
                                     x=self.x - self.width / 2 + 20 * ECHELLE,
                                     y=self.y,
                                     width=self.width / 2 * ECHELLE,
                                     height=self.height * ECHELLE,
                                     anchor_x='left',
                                     anchor_y='center',
                                     batch=self.batch,
                                     group=groupes['textes'],
                                     color=(255, 255, 255, 255),
                                     font_size=20,
                                     )
        self.action.content_valign = 'center'

        self.commande = elements.Label(text=commande.upper(),
                                       x=self.x + self.width / 4 * ECHELLE,
                                       y=self.y,
                                       width=self.width / 2 * ECHELLE,
                                       height=self.height * ECHELLE,
                                       anchor_x='center',
                                       anchor_y='center',
                                       batch=self.batch,
                                       group=groupes['textes'],
                                       color=(0, 0, 0, 255),
                                       font_size=20,
                                       )

    def normal(self) :
        self.image = ressources.images.controles['normal']
        self.action.color = (255, 255, 255, 255)
        self.commande.color = (0, 0, 0, 255)

    def focus(self) :
        self.image = ressources.images.controles['focus']
        self.action.color = (0, 184, 0, 255)
        self.commande.color = (255, 255, 255, 255)

    def delete(self) :
        self.action.delete()
        self.commande.delete()
        super().delete()


class LabelOption(elements.Label) :
    def __init__(self, **dargs) :
        options = dict(color=(255, 255, 255, 255),
                       font_name=ressources.police,
                       height=40,
                       anchor_y='center',
                       font_size=20,
                       )
        options.update(dargs)
        super().__init__(**options)
        self.content_valign = 'center'

    def focus(self) :
        self.font_size = self._tailles[1] * ECHELLE
        self.color = self._couleurs[1]

    def normal(self) :
        self.font_size = self._tailles[0] * ECHELLE
        self.color = self._couleurs[0]


class Options :
    def __init__(self, batch, groupes, retour) :
        self._retour = retour
        self._sons = audio.Sons()
        self._musiques = audio.Musiques
        x = LARGEUR_FENETRE / 2
        ys = [int(n * ECHELLE) for n in (670, 590, 510, 180, 90)]
        self._config = config.Config()

        self._labels = (elements.ItemMenu(text='SONS',
                                          x=LARGEUR_FENETRE / 2 - 20,
                                          y=ys[0],
                                          batch=batch,
                                          anchor_x='right',
                                          group=groupes['textes']),
                        elements.ItemMenu(text='MUSIQUES',
                                          x=LARGEUR_FENETRE / 2 - 20,
                                          y=ys[1],
                                          anchor_x='right',
                                          batch=batch,
                                          group=groupes['textes']),
                        elements.ItemMenu(text='CONTRÔLES',
                                          x=LARGEUR_FENETRE / 2,
                                          y=ys[2],
                                          anchor_x='center',
                                          batch=batch,
                                          group=groupes['textes']),
                        elements.ItemMenu(text='retour au menu',
                                          y=ys[4],
                                          batch=batch,
                                          group=groupes['textes']),
                        )
        controles = []
        self._controles = {}
        y = ys[3]
        self._bords = []
        lcmd = list(self._config.commandes)[::-1]
        ba = (0, len(lcmd)-1)
        for i, n in enumerate(lcmd) :
            cmd = pg.window.key._key_names[getattr(self._config, n)]
            self._controles[n] = cmd
            if i in ba :
                bord = elements.Sprite(ressources.images.controles['bords'],
                                       x=x,
                                       y=y,
                                       batch=batch,
                                       group=groupes['selements'])
                self._bords.append(bord)
            controles.append(itemOptionControles(action=n,
                                                 commande=cmd,
                                                 groupes=groupes,
                                                 x=x,
                                                 y=y,
                                                 batch=batch))
            y += 50 * ECHELLE
        
        self._options = (itemOption(y=ys[0], batch=batch, groupes=groupes),
                         itemOption(y=ys[1], batch=batch, groupes=groupes),
                         controles,
                        )

        if not self._config.sons :
            self._options[0].desactiver()

        if not self._config.musiques :
            self._options[1].desactiver()
        self._editionCtrl = False
        self._focus = 0
        self._labels[self._focus].focus()
        self._nbCtrl = len(controles)
        self._nbOptions = len(self._labels)

    def _initCtrls(self) :
        for n in self._options[2] :
            n.normal()

    def on_key_press(self, sym, mod) :
        if sym == pg.window.key.TAB and not self._editionCtrl :
            self._sons.nav.play()
            self._initCtrls()
            self._labels[self._focus].normal()
            if self._focus == self._nbOptions - 1 :
                self._focus = 0
            else :
                self._focus += 1
            if self._focus == 2 :
                self._ctrlFocus = self._nbCtrl - 1
                self._options[2][self._ctrlFocus].focus()
            self._labels[self._focus].focus()
        elif self._focus in (0, 1) :
            nom = self._labels[self._focus].text.lower()
            if sym == pg.window.key.LEFT :
                getattr(self, '_' + nom).activer()
                self._sons.clic.play()
                setattr(self._config, nom, True)
                self._options[self._focus].activer()
                
            elif sym == pg.window.key.RIGHT :
                self._sons.clic.play()
                getattr(self, '_' + nom).desactiver()
                setattr(self._config, nom, False)
                self._options[self._focus].desactiver()
        elif self._focus == 2 :
            if not self._editionCtrl :
                if sym == pg.window.key.UP :
                    self._sons.navCmd.play()
                    self._options[2][self._ctrlFocus].normal()
                    if self._ctrlFocus == self._nbCtrl - 1 :
                        self._ctrlFocus = 0
                    else :
                        self._ctrlFocus += 1
                    self._options[2][self._ctrlFocus].focus()
                elif sym == pg.window.key.DOWN :
                    self._sons.navCmd.play()
                    self._options[2][self._ctrlFocus].normal()
                    if self._ctrlFocus == 0 :
                        self._ctrlFocus = self._nbCtrl - 1
                    else :
                        self._ctrlFocus -= 1
                    self._options[2][self._ctrlFocus].focus()
                elif sym == pg.window.key.ENTER :
                    self._sons.clic.play()
                    self._options[2][self._ctrlFocus]
                    self._editionCtrl = True
                    self._ctrlActuel = self._options[2][self._ctrlFocus].commande.text
                    self._options[2][self._ctrlFocus].commande.text = ''
            else :
                nom = self._options[2][self._ctrlFocus].action.text.lower()
                try :
                    setattr(self._config, nom, sym)
                except :
                    pass
                else :
                    self._sons.clic.play()
                    num = getattr(self._config, nom)
                    self._controles[nom] = num
                    commande = pg.window.key._key_names[num]
                    self._options[2][self._ctrlFocus].commande.text = commande
                    self._editionCtrl = False
        elif self._focus == 3 :
            self._sons.clic.play()
            self._retour()
        
    def delete(self) :
        for n in self._bords :
            n.delete()
        for n in self._labels :
            n.delete()
        for n in self._options[:2] :
            n.delete()
        for n in self._options[2] :
            n.delete()
