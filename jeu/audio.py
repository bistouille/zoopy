'''
This file is part of « zoopy ».

« zoopy » is free software : 
You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

« zoopy » is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
« zoopy ».

If not, see <http://www.gnu.org/licenses/>.
'''

import pyglet as pg

from .const import *
from . import bibli, config

conf = config.Config()

pg.options['audio'] = AUDIO
# L'utilisation d'autres format de sons que wav nécessite d'installer avbin.
# http://avbin.github.io/AVbin/Download.html

EXTSONS = '.ogg' if pg.media.have_avbin else '.wav'

class StaticSource(pg.media.StaticSource) :
    def __init__(self, source) :
        super().__init__(source)
        self._aplay = self.play
        self._iplay = lambda : None

    def activer(self) :
        self.play = self._aplay

    def desactiver(self) :
        self.play = self._iplay
    
class Sons(metaclass=bibli.Singleton) :
    noms = ('clic',
            'nav',
            'navCmd',
            'heurt',
            'echange',
            'niveau',
            'score',
            'explosionPiece',
            'tir',
            'gagne',
            'perdu',
            'debutNiveau',
            'feu',
            )

    def __init__(self) :
        conf = config.Config()
        self.__dict__['sons'] = dict()
        for n in self.noms :
            nn = ''.join(c if c.islower() else '_' + c.lower() for c in n)
            f = RSONS + nn + EXTSONS
            self.__dict__['sons'][n] = StaticSource(pg.media.load(f, streaming=False))
                
        if not conf.sons :
            self.desactiver()

    def __getattr__(self, nom) :
        return self.__dict__['sons'][nom]

    def activer(self) :
        for n in self.__dict__['sons'].values() :
            n.activer()

    def desactiver(self) :
        for n in self.__dict__['sons'].values() :
            n.desactiver()

class Muet :
    def __getattr__(self, nom) :
        return lambda s=self : s

    def __call__(self) :
        pass

class Lecteur :
    def __init__(self) :
        self.__dict__['_amusiques'] = pg.media.Player()
        self.__dict__['_imusiques'] = Muet()
        if conf.musiques :
            self.activer()
        else :
            self.desactiver()
        
    def __getattr__(self, nom) :
        return getattr(self.__dict__['_musiques'], nom)

    def activer(self) :
        self.__dict__['_musiques'] = self.__dict__['_amusiques']

    def desactiver(self) :
        self.__dict__['_musiques'] = self.__dict__['_imusiques']

class Musiques :
    instances = []
    def __init__(self, musiques) :
        self.musiques = []
        for m in musiques :
            m = pg.media.load(RMUSIQUES + m + EXTSONS)
            self.musiques.append(m)
        # Même format audio impératif pour tous les fichiers sons
        self._lecture = pg.media.SourceGroup(self.musiques[0].audio_format,
                                             None
                                             )
        self._lecture.loop = True
        for m in self.musiques :
            self._lecture.queue(m)
        
        self.lecteur = Lecteur()
        self.lecteur.queue(self._lecture)
        
        if not conf.musiques :
            self.lecteur.desactiver()
            
        self.instances.append(self)
        
    def jouer(self) :
        self.lecteur.play()
        
    def arreter(self) :
        self.lecteur.pause()
        
    def suivant(self) :
        self.lecteur.next_source()

    def delete(self) :
        self.lecteur.delete()
        self.instances.remove(self)

    @classmethod
    def activer(c) :
        for n in c.instances :
            n.lecteur.activer()
            if not n.lecteur.source :
                n.lecteur.queue(n._lecture)
            n.jouer()

    @classmethod
    def desactiver(c) :
        for n in c.instances :
            n.arreter()
            n.lecteur.desactiver()
