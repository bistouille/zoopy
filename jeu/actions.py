'''
This file is part of « zoopy ».

« zoopy » is free software : 
You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

« zoopy » is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
« zoopy ».

If not, see <http://www.gnu.org/licenses/>.
'''

import pyglet as pg

class Actions :
    def __init__(self) :
        self._actions = set()

    def schedule(self, f, *args, **dargs) :
        self._actions.add(f)
        pg.clock.schedule(f, *args, **dargs)

    def schedule_interval(self, f, intervalle, *args, **dargs) :
        self._actions.add(f)
        pg.clock.schedule_interval(f, intervalle, *args, **dargs)

    def schedule_once(self, f, delai, *args, **dargs) :
        self._actions.add(f)
        pg.clock.schedule_once(f, delai, *args, **dargs)

    def schedule_interval_soft(self, func, intervalle, *args, **kwargs) :
        self._actions.add(f)
        pg.clock.schedule_interval_soft(f, f, intervalle, *args, **dargs)

    def unschedule(self, f=None) :
        if f :
            try :
                self._actions.remove(f)
                pg.clock.unschedule(f)
            except KeyError :
                pass
        else :
            for n in self._actions :
                pg.clock.unschedule(n)
            self._actions.clear()
