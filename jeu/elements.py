'''
This file is part of « zoopy ».

« zoopy » is free software : 
You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

« zoopy » is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
« zoopy ».

If not, see <http://www.gnu.org/licenses/>.
'''

import pyglet as pg

from .const import *
from . import audio, actions, ressources

def animation(image,
              lignes:int,
              colonnes:int,
              largeur:int,
              hauteur:int,
              delai:float,
              repetition=False,
              ) :
    grid = pg.image.ImageGrid(image,
                              rows=lignes,
                              columns=colonnes,
                              item_width=largeur,
                              item_height=hauteur)
    animation = pg.image.Animation.from_image_sequence(grid, delai, repetition)
    return animation

class Sprite(pg.sprite.Sprite) :
    def __init__(self, image, **dargs) :
        super().__init__(image, **dargs)
        self.scale = ECHELLE


class Label(pg.text.Label) :
    def __init__(self, **dargs) :
        try :
            dargs['font_size'] *= ECHELLE
        except KeyError :
            pass
        dargs['font_name'] = ressources.police
        super().__init__(**dargs)


class FondScores(Sprite) :
    def __init__(self, **dargs) :
        super().__init__(ressources.images.scores, **dargs)


class FondJeu(Sprite) :
    def __init__(self, **dargs) :
        super().__init__(ressources.images.plateau, **dargs)


class FondMenu(Sprite) :
    def __init__(self, **dargs) :
        super().__init__(ressources.images.menu, **dargs)


class Logo(Sprite) :
    def __init__(self, **dargs) :
        y = int(800 * ECHELLE)
        x = LARGEUR_FENETRE / 2
        super().__init__(ressources.images.logo, x=x, y=y, **dargs)

class Niveau(Label) :
    def __init__(self, batch, group, **dargs) :
        x = LARGEUR_FENETRE - TB * 3.5
        y = TB * 2.5
        self._actions = actions.Actions()
        super().__init__(x=x,
                         y=y,
                         batch=batch,
                         group=group,
                         color=(66, 66, 66, 0),
                         font_size=32,
                         anchor_x='center',
                         anchor_y='center',
                         **dargs
                         )
        self.content_valign = 'center'

    def _formaterTexte(self, niveau:int) :
        return 'niveau {}'.format(NIVEAUX[niveau][0]).upper()

    def changer(self, niveau:int) :
        self.text = self._formaterTexte(niveau)

    def cacher(self) :
        self._actions.schedule_interval(self._assombrir, .1)

    def montrer(self) :
        self._actions.schedule_interval(self._eclaircir, .1)

    def _eclaircir(self, t) :
        self._changerAlpha(15)
        if self.color[3] >= 255 :
            self._actions.unschedule(self._eclaircir)

    def _assombrir(self, t) :
        self._changerAlpha(-15)
        if self.color[3] <= 0 :
            self._actions.unschedule(self._assombrir)

    def _changerAlpha(self, pas) :
        c = list(self.color[:3])
        c.append(self.color[3] + pas)
        self.color = tuple(c)


class Information(Sprite) :
    def __init__(self, text, batch, groupes, **dargs) :
        x = LARGEUR_FENETRE / 2
        y = HAUTEUR_FENETRE / 2
        super().__init__(ressources.images.boutons['normal'],
                         batch=batch,
                         group=groupes['info'],
                         x=x,
                         y=y,
                         **dargs)
        self.anchor_x = x
        self.anchor_y = y
        self.scale *= 2
        self.opacity = 192
        self._c = text.upper()
        self._texte = Label(text=self._c,
                            x=x,
                            y=y,
                            batch=batch,
                            color=(66, 66, 66, 255),
                            font_size=32,
                            width=self.width,
                            height=self.height,
                            anchor_x='center',
                            anchor_y='center',
                            group=groupes['textes'],
                           )
        self._texte.content_valign = 'center'
        
    @property
    def visible(self) :
        return self._visible

    @visible.setter
    def visible(self, b) :
        self._set_visible(b)
        self._texte.text = self._c if b else ''

    def delete(self) :
        self._texte.delete()
        try :
            super().delete()
        except AttributeError :
            pass


class Chiffre(Label) :
    def __init__(self, x, y, text, batch, group) :
        super().__init__(text=text,
                         x=x,
                         y=y,
                         batch=batch,
                         color=(100, 100, 100, 255),
                         font_size=32,
                         anchor_x='center',
                         anchor_y='center',
                         group=group,
                         )

 
class ItemMenu(Label) :
    def __init__(self, text, y, batch, group, **dargs) :
        self._couleurs = ((99, 99, 99, 255),
                          (255, 255, 255, 255))
        self._tailles = (24, 28)
        x = LARGEUR_FENETRE / 2
        options = dict(text=text.upper(),
                        batch=batch,
                        group=group,
                        x=x,
                        y=y,
                        anchor_x='center',
                        anchor_y='center',
                        )
        options.update(dargs)
        super().__init__(**options)
        self.normal()

    def focus(self) :
        self.font_size = self._tailles[1] * ECHELLE
        self.color = self._couleurs[1]

    def normal(self) :
        self.font_size = self._tailles[0] * ECHELLE
        self.color = self._couleurs[0]

class Menu :
    def __init__(self, items, batch, group) :
        self._sons = audio.Sons()
        self.items = []
        self.cmds = []
        y = TB * .75 * len(items)
        for n, c in items :
            self.items.append(ItemMenu(text=n,
                                        y=y,
                                        batch=batch,
                                        group=group,
                                        ))
            self.cmds.append(c)
            y -= TB * .75
        self._selection = 0

    def on_key_release(self, sym, mod) :
        if sym == pg.window.key.TAB :
            self._sons.nav.play()
            self.items[self.selection].normal()
            self._selection = 1 if not self._selection else 0
            self.items[self.selection].focus()
        elif sym == pg.window.key.ENTER :
            self._sons.clic.play()
            self.cmds[self.selection]()

    @property
    def selection(self) :
        return self._selection

    @selection.setter
    def selection(self, i) :
        try :
            self.items[i].focus()
        except IndexError :
            pass
        else :
            self._selection = i

    def delete(self) :
        for n in self.items :
            n.delete()


class Feu(Sprite) :
    def __init__(self, batch, group) :
        anim = animation(ressources.images.feu,
                         lignes=3,
                         colonnes=5,
                         largeur=256,
                         hauteur=300,
                         delai=0.05,
                         repetition=True)
        super().__init__(anim,
                         x=TB*7,
                         y=TB*5,
                         batch=batch,
                         group=group,)

class CouleurCentre(Sprite) :
    def __init__(self, couleur, batch, group) :
        super().__init__(ressources.images.fondsCentre[couleur],
                         x=LARGEUR_FENETRE / 2,
                         y=HAUTEUR_FENETRE / 2,
                         batch=batch,
                         group=group,)


