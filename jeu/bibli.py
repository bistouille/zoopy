'''
This file is part of « zoopy ».

« zoopy » is free software : 
You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

« zoopy » is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
« zoopy ».

If not, see <http://www.gnu.org/licenses/>.
'''

import pyglet as pg
import time
import os
import pickle
from math import ceil, sqrt

from .const import *

def bornes(pos, aire) :
    pos += 1
    bd = ceil(pos / aire) * aire
    bg = bd - aire
    return (bg, bd-1)

def timestamp() :
    return round(time.time())

def deplacement(origine, destination, pas=TB2) :
    distance = (destination.x - origine.x, destination.y - origine.y)
    n = sqrt(distance[0] ** 2 + distance[1] ** 2)
    direction = (distance[0] / n, distance[1] / n)
    x = direction[0] * pas
    y = direction[1] * pas
    return Point(x, y)

def chargerScore(fichier:str) -> list :
    with open(fichier, 'rb') as f :
        pr = pickle.Unpickler(f)
        donnees = pr.load()
        return donnees

def enregistrerScore(fichier:str, donnees:list) -> None :
    with open(fichier, 'wb') as f :
        pp = pickle.Pickler(f)
        pp.dump(donnees)


class Point :
    __slots__ = ('x', 'y')
    def __init__(self, x, y) :
        self.x = x
        self.y = y

    def __str__(self) :
        return 'x: {}, y:{}'.format(self.x, self.y)

    def __eq__(self, point) :
        return self.x == point.x and self.y == point.y

    def __ne__(self, point) :
        return self.x != point.x and self.y != point.y
        
    def __le__(self, point) :
        return self.x <= point.x or self.y <= point.y
    
    def __lt__(self, point) :
        return self.x < point.x and self.y < point.y
        
    def __ge__(self, point) :
        return self.x >= point.x or self.y >= point.y
        
    def __gt__(self, point) :
        return self.x > point.x and self.y > point.y


class Scores :
    dernier = -1
    def __init__(self, top=5) :
        self.top = 5
        if not os.path.isfile(FSCORES) :
            self._scores = []
            for _ in range(self.top) :
                self._scores.append(['.....', 0, 0, 0])
        else :
            self.charger()

    def enregistrer(self, score:int, depart:int, fin:int) :
        Scores.dernier = -1
        for i, n in enumerate(self._scores) :
            if score > n[1] :
                break
        
        self._scores.insert(i, ['.....', score, depart, fin])
        enregistrerScore(FSCORES, self._scores)
        
        Scores.dernier = i if i < self.top - 1 else -1

    def charger(self) :
        self._scores = chargerScore(FSCORES)

    def changerNom(self, nom, index) :
        self._scores[index][0] = nom
        enregistrerScore(FSCORES, self._scores)

    @property
    def scores(self) :
        return self._scores[:self.top]
        

class Elements :
    def __init__(self) :
        self.__dict__['elements'] = {}

    @property
    def elements(self) :
        return self.__dict__['elements']

    def __getitem__(self, cle) :
        return self.cle

    def __setitem__(self, cle, valeur) :
        self.__dict__['elements'][cle] = valeur

    def __getattr__(self, cle) :
        return self.__dict__['elements'][cle]

    def __setattr__(self, cle, valeur) :
        self.__dict__['elements'][cle] = valeur

    def __delattr__(self, cle) :
        self.__dict__['elements'][cle].delete()
        del(self.__dict__['elements'][cle])

    def clear(self) :
        for el in self.__dict__['elements'].values() :
            el.delete()
        self.__dict__['elements'].clear()


class Singleton(type):
    def __init__(self, *args, **dargs) :
        self._instance = None
        super().__init__(*args, **dargs)

    def __call__(self, *args, **dargs) :
        if not self._instance :
            self._instance = super().__call__(*args, **dargs)
        return self._instance
