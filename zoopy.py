'''
This file is part of « zoopy ».

« zoopy » is free software : 
You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

« zoopy » is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
« zoopy ».

If not, see <http://www.gnu.org/licenses/>.
'''

import pyglet as pg

from jeu.const import *
from jeu import audio, base, bibli, ressources


class Zoopy(pg.window.Window) :
    def __init__(self) :
        pg.clock.set_fps_limit(FPS)
        super().__init__(width=LARGEUR_FENETRE,
                         height=HAUTEUR_FENETRE,
                         caption='Zoopy'
                         )
        self.set_icon(ressources.images.icone)
        self.batch = pg.graphics.Batch()
        self.groupes = dict(fond=pg.graphics.OrderedGroup(0),
                            sfond=pg.graphics.OrderedGroup(1),
                            selements=pg.graphics.OrderedGroup(3),
                            elements=pg.graphics.OrderedGroup(5),
                            info=pg.graphics.OrderedGroup(8),
                            textes=pg.graphics.OrderedGroup(10),
                            )
        self.elements = bibli.Elements()
        self.musique = audio.Musiques(MUSIQUE_MENU)

    def on_draw(self) :
        self.clear()
        self.batch.draw()

    @property
    def ecran(self) :
        return self._ecran

    @ecran.setter
    def ecran(self, cls) :
        try :
            self._ecran.delete()
        except AttributeError :
            pass
        self.elements.clear()
        self._ecran = cls(self)
    
    def jouer(self) :
        self.musique.arreter()    
        self.ecran = base.ZoopyJeu

    def scores(self) :
        self.musique.jouer()
        self.ecran = base.ZoopyScore

    def menu(self) :
        self.musique.jouer()
        self.ecran = base.ZoopyMenu

    def options(self) :
        self.musique.jouer()
        self.ecran = base.ZoopyOptions

    def quitter(self) :
        pg.app.exit()

if __name__ == '__main__':
    zoopy = Zoopy()
    zoopy.menu()
    pg.app.run()
