# Clone du jeu zoop réalisé avec python3 et pyglet

## Prérequis

- python 3.4
- pyglet (pip3 install pyglet)
- avbin (http://avbin.github.io/AVbin/Download.html)

## Lancement du jeu

Soit en lancant le script zoopy.py depuis votre terminal ou console, soit en ouvrant le script directement avec python3.

## But du jeu

Le but du jeu est simple, vous êtes sur la zone centrale du plateau du jeu et vous devez absolument empêcher les pièces ennemies arrivant des 4 côtés du plateau de pénétrer sur cette zone centrale, autrement le jeu sera terminé et vous aurez perdu.

Vous ne pouvez éliminer que les pièces ennemies ayant la même couleur que vous, si vous frappez un ennemi ayant une couleur différente, vous échangerez votre couleur avec cet ennemi.

Il y a en tout 9 niveaux à franchir pour gagner la partie, l'apparition des pièces ennemies se faisant plus rapidement au fil des niveaux, les derniers niveaux s'avèrent être ardus à franchir mais néanmoins possible.

Plus vous éliminez de pièces ennemies en un tir, plus vous marquez de points, le but final étant de scorer un maximum.